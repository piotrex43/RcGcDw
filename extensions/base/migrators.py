#  This file is part of Recent changes Goat compatible Discord webhook (RcGcDw).
#
#  RcGcDw is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  RcGcDw is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with RcGcDw.  If not, see <http://www.gnu.org/licenses/>.
from src.api import formatter
from src.api.util import embed_helper, sanitize_to_url, compact_author
from src.discord.message import DiscordMessage
from src.api.context import Context


# newusers/reclaim - New user reclaimed
@formatter.embed(event="newusers/reclaim", mode="embed")
def embed_newusers_reclaim(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    embed_helper(ctx, embed, change)
    embed["url"] = ctx.client.create_article_path(sanitize_to_url("User:{}".format(change["user"])))
    embed["title"] = ctx._("Reclaimed account")
    return embed


@formatter.compact(event="newusers/reclaim")
def compact_newusers_reclaim(ctx: Context, change: dict):
    author, author_url = compact_author(ctx, change)
    content = ctx._("Account [{author}]({author_url}) was reclaimed").format(author=author, author_url=author_url)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)


# newusers/migrated - New user migrated
@formatter.embed(event="newusers/migrated", mode="embed")
def embed_newusers_migrated(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    embed_helper(ctx, embed, change)
    embed["url"] = ctx.client.create_article_path(sanitize_to_url("User:{}".format(change["user"])))
    embed["title"] = ctx._("Migrated account")
    return embed


@formatter.compact(event="newusers/migrated")
def compact_newusers_migrated(ctx: Context, change: dict):
    author, author_url = compact_author(ctx, change)
    content = ctx._("Account [{author}]({author_url}) was migrated").format(author=author, author_url=author_url)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)
