��          �      l      �     �     �  "        6     D     T     b  	   q  
   {  "   �  "   �  (   �     �  	                  0     D     K  
   `  �  k     �                  !   1     S     f     s     �     �     �     �     �     �               *     =     J     b                                      	                                           
                  ({} action)  ({} actions)  ({} edit)  ({} edits)  UTC ({} action)  UTC ({} actions) Admin actions But nobody came Bytes changed Daily overview Day score Edits made Most active hour Most active hours Most active user Most active users Most edited article Most edited articles New articles New files No activity No description provided Unique contributors hidden {value} (avg. {avg}) ~~hidden~~ Project-Id-Version: Japanese (RcGcDw)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Japanese <https://translate.wikibot.de/projects/rcgcdw/main/ja/>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.12.1
  ({} 行動)  ({} 編集)  UTC ({} 行動) 管理者の行動 しかし誰も来なかった。 バイトの変更 日別概要 日間スコア 編集内容 最も活動的な時間 最も活動的なユーザー 最も編集された記事 新規記事 新規ファイル 活動なし 説明はありません 唯一の編集者 隠された {value} (平均. {avg}) ~~隠された~~ 