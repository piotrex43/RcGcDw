#  This file is part of Recent changes Goat compatible Discord webhook (RcGcDw).
#
#  RcGcDw is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  RcGcDw is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with RcGcDw.  If not, see <http://www.gnu.org/licenses/>.

from src.discord.message import DiscordMessage
from src.api import formatter
from src.api.context import Context
from src.api.util import embed_helper, compact_author, sanitize_to_markdown, sanitize_to_url, clean_link
from src.misc import LinkParser

# Approved Revs - https://mediawiki.org/wiki/Extension:Approved_Revs
# approval/approvefile

@formatter.embed(event="approval/approvefile")
def embed_approval_approvefile(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    embed_helper(ctx, embed, change)
    embed["url"] = ctx.client.create_article_path(sanitize_to_url(change["title"]))
    embed["title"] = ctx._("Approved a file revision of {file}").format(file=sanitize_to_markdown(change["title"]))
    link_parser_object = LinkParser(ctx.client.WIKI_JUST_DOMAIN)
    link_parser_object.feed(change.get("logparams", {}).get("0", ""))
    embed["description"] = ctx._("File version from {time} got approved").format(name=change["title"], time=link_parser_object.new_string)
    # TODO Make timestamp more user friendly? Getting user who uploaded will be a pain though, same with approval/approve
    return embed


@formatter.compact(event="approval/approvefile")
def compact_approval_approvefile(ctx: Context, change: dict):
    author, author_url = compact_author(ctx, change)
    link = clean_link(ctx.client.create_article_path(sanitize_to_url(change["title"])))
    content = ctx._("[{author}]({author_url}) approved a file revision of [{file_name}]({file_url})").format(author=author,
                                                                                                    author_url=author_url,
                                                                                                    file_name=sanitize_to_markdown(change[
                                                                                                        "title"]),
                                                                                                    file_url=link)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)

# approval/approve
@formatter.embed(event="approval/approve")
def embed_approval_approve(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    embed_helper(ctx, embed, change)
    embed["url"] = ctx.client.create_article_path(sanitize_to_url(change["title"]))
    embed["title"] = ctx._("Approved a revision of {article}").format(article=sanitize_to_markdown(change["title"]))
    link_parser_object = LinkParser(ctx.client.WIKI_JUST_DOMAIN)
    link_parser_object.feed(change.get("logparams", {}).get("0", ""))
    embed["description"] = ctx._("Revision number {revision_id} got approved").format(name=change["title"], time=link_parser_object.new_string)
    return embed


@formatter.compact(event="approval/approve")
def compact_approval_approve(ctx: Context, change: dict):
    author, author_url = compact_author(ctx, change)
    link = clean_link(ctx.client.create_article_path(sanitize_to_url(change["title"])))
    content = ctx._("[{author}]({author_url}) approved a revision of [{article}]({article_url})").format(author=author,
                                                                                                    author_url=author_url,
                                                                                                    article=sanitize_to_markdown(change[
                                                                                                        "title"]),
                                                                                                    article_url=link)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)

# approval/unapprove
@formatter.embed(event="approval/unapprove")
def embed_approval_approve(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    embed_helper(ctx, embed, change)
    embed["url"] = ctx.client.create_article_path(sanitize_to_url(change["title"]))
    embed["title"] = ctx._("Unapproved revision of {article}").format(article=sanitize_to_markdown(change["title"]))
    return embed


@formatter.compact(event="approval/unapprove")
def compact_approval_approve(ctx: Context, change: dict):
    author, author_url = compact_author(ctx, change)
    link = clean_link(ctx.client.create_article_path(sanitize_to_url(change["title"])))
    content = ctx._("[{author}]({author_url}) unapproved a revision of [{article}]({article_url})").format(author=author,
                                                                                                    author_url=author_url,
                                                                                                    article=sanitize_to_markdown(change[
                                                                                                        "title"]),
                                                                                                    article_url=link)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)
