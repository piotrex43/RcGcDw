#  This file is part of Recent changes Goat compatible Discord webhook (RcGcDw).
#
#  RcGcDw is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  RcGcDw is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with RcGcDw.  If not, see <http://www.gnu.org/licenses/>.

from src.discord.message import DiscordMessage
from src.api import formatter
from src.api.context import Context
from src.api.util import embed_helper, clean_link, compact_author, compact_summary, sanitize_to_markdown


# GlobalBlocking - https://www.mediawiki.org/wiki/Extension:GlobalBlocking
# gblblock/gblock2 - Globally block an IP address

@formatter.embed(event="gblblock/gblock2", mode="embed")
def embed_gblblock_gblock2(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    user = change["title"].split(':', 1)[1]
    embed["url"] = ctx.client.create_article_path("Special:Contributions/{user}".format(user=user))
    embed["title"] = ctx._("Globally blocked {blocked_user} ({options})").format(blocked_user=user, options=change["logparams"].get("0", ctx._("no further information about the block")))
    embed_helper(ctx, embed, change)
    return embed


@formatter.compact(event="gblblock/gblock2", mode="compact")
def compact_gblblock_gblock2(ctx: Context, change: dict):
    user = change["title"].split(':', 1)[1]
    author, author_url = compact_author(ctx, change)
    parsed_comment = compact_summary(ctx)
    link = clean_link(ctx.client.create_article_path("Special:Contributions/{user}".format(user=user)))
    content = ctx._(
        "[{author}]({author_url}) globally blocked [{user}]({user_url}) {options} {comment}").format(author=author,
                                                                                                          author_url=author_url,
                                                                                                          user=sanitize_to_markdown(user),
                                                                                                          options=change["logparams"].get("0", ctx._("no further information about the block")),
                                                                                                          user_url=link,
                                                                                                          comment=parsed_comment)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)

# gblblock/whitelist - Disable locally a global ban on an IP address

@formatter.embed(event="gblblock/whitelist", mode="embed")
def embed_gblblock_whitelist(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    user = change["title"].split(':', 1)[1]
    embed["url"] = ctx.client.create_article_path("Special:Contributions/{user}".format(user=user))
    embed["title"] = ctx._("Disabled the global block locally on {blocked_user}").format(blocked_user=user)
    embed_helper(ctx, embed, change)
    return embed


@formatter.compact(event="gblblock/whitelist", mode="compact")
def compact_gblblock_whitelist(ctx: Context, change: dict):
    user = change["title"].split(':', 1)[1]
    author, author_url = compact_author(ctx, change)
    parsed_comment = compact_summary(ctx)
    link = clean_link(ctx.client.create_article_path("Special:Contributions/{user}".format(user=user)))
    content = ctx._(
        "[{author}]({author_url}) disabled the global block locally on [{user}]({user_url}) {comment}").format(author=author,
                                                                                  author_url=author_url,
                                                                                  user=sanitize_to_markdown(user),
                                                                                  user_url=link,
                                                                                  comment=parsed_comment)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)

# gblblock/gunblock - Remove a global block on an IP address


@formatter.embed(event="gblblock/gunblock", mode="embed")
def embed_gblblock_gunblock(ctx: Context, change: dict):
    embed = DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url)
    user = change["title"].split(':', 1)[1]
    embed["url"] = ctx.client.create_article_path("Special:Contributions/{user}".format(user=user))
    embed["title"] = ctx._("Removed the global block on {blocked_user}").format(blocked_user=user)
    embed_helper(ctx, embed, change)
    return embed


@formatter.compact(event="gblblock/gunblock", mode="compact")
def compact_gblblock_gunblock(ctx: Context, change: dict):
    user = change["title"].split(':', 1)[1]
    author, author_url = compact_author(ctx, change)
    parsed_comment = compact_summary(ctx)
    link = clean_link(ctx.client.create_article_path("Special:Contributions/{user}".format(user=user)))
    content = ctx._(
        "[{author}]({author_url}) removed the global block on [{user}]({user_url}) {comment}").format(author=author,
                                                                                  author_url=author_url,
                                                                                  user=sanitize_to_markdown(user),
                                                                                  user_url=link,
                                                                                  comment=parsed_comment)
    return DiscordMessage(ctx.message_type, ctx.event, ctx.webhook_url, content=content)
